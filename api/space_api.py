class TestspaceSpaceApi:
    def __init__(self, result_json):
        self.result_json = result_json

    def get_space_list(self):
        space_name_list = []
        for space in self.result_json:
            space_name_list.append(space['name'])
        return space_name_list

    def get_space_id_by_name(self, space_name):
        for space in self.result_json:
            if space['name'] == space_name:
                return space['id']
        return None

    def get_space_name_by_id(self, space_id):
        for space in self.result_json:
            if space['id'] == space_id:
                return space['name']
        return None
