class TestspaceProjectApi:
    def __init__(self, result_json):
        self.result_json = result_json

    def get_project_list(self):
        project_name_list = []
        for project in self.result_json:
            project_name_list.append(project['name'])
        return project_name_list

    def get_project_id_by_name(self, project_name):
        for project in self.result_json:
            if project['name'] == project_name:
                return project['id']
        return None

    def get_project_name_by_id(self, project_id):
        for project in self.result_json:
            if project['id'] == project_id:
                return project['name']
        return None
