import requests_mock
from api import space_result_set_api
from api import testspace_rest_query
import json
import pytest


def load_request_url(self):
    with open("pytest/spaces_result_set.txt") as myfile:
        self.request_text = "".join(line.rstrip() for line in myfile)

    self.request_json = json.loads(self.request_text)
    self.request_url = 'https://testspace.com/api/spaces/829/result_sets'


class TestSpaceResultSetApi:
    @pytest.fixture(autouse=True)
    def setup_class(self):
        load_request_url(self)

    def test_number_result_sets(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        assert len(self.request_json) == results_sets.get_number_results()

    def test__result_set_name_list(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        assert len(self.request_json) == len(results_sets.get_list_result_names())

    def test__result_set_by_name(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        results_set = results_sets.get_result_by_name("c9.Build")
        assert 7355 == results_set['id']

    def test__result_set_by_name_invalid(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        results_set = results_sets.get_result_by_name("build")
        assert results_set is None

    def test__result_set_last_updated(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            result_response = testspace_rest_query.testspace_get(self.request_url)
        results_sets = space_result_set_api.TestspaceSpaceResultSetApi(result_response.json())
        results_set = results_sets.get_last_updated()
        assert 8722 == results_set['id']
