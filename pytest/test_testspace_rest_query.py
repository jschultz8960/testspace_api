import json
import pytest
import requests.exceptions

import requests_mock
from api import testspace_rest_query
from api import result_set_api
from api import space_result_set_api


def load_request_url(self):
    with open("pytest/spaces_result_set.txt") as myfile:
        space_results_string = "".join(line.rstrip() for line in myfile)

    self.space_results_json = json.loads(space_results_string)
    self.request_text = json.dumps(self.space_results_json)
    self.request_url = 'https://testspace.com/api/spaces/829/result_sets'


class TestTestspaceRestQuery:
    @pytest.fixture(autouse=True)
    def setup_class(self):
        load_request_url(self)

    def test_request_get_404(self):
        response = testspace_rest_query.testspace_get(self.request_url)
        assert response.status_code == 404

    def test_request_get_200(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url)
            response = testspace_rest_query.testspace_get(self.request_url)
        assert response.status_code == 200

    def test_request_post_201(self):
        with requests_mock.Mocker() as mock:
            payload = '{"payload": "None"}'
            mock.post(self.request_url, json=payload, status_code=201)
            response = testspace_rest_query.testspace_post(self.request_url, payload)
        assert response.status_code == 201

    def test_space_result_sets(self):
        url = 'https://ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS:@abc.testspace.com' \
              '/api/projects/project1/spaces/space1/result_sets'
        with requests_mock.Mocker() as mock:
            mock.get(url, text=self.request_text)
            response = testspace_rest_query.space_result_sets('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                              'abc',
                                                              'project1',
                                                              'space1',
                                                              enterprise=None)
        result_sets = space_result_set_api.TestspaceSpaceResultSetApi(response.json())
        assert len(self.space_results_json) == result_sets.get_number_results()

    def test_request_get_invalid_url_timeout(self):
        with pytest.raises(requests.exceptions.HTTPError):
            testspace_rest_query.testspace_get('https://testsce.com/')

    def test_request_post_invalid_url_timeout(self):
        with pytest.raises(requests.exceptions.HTTPError):
            payload = '{"payload": "None"}'
            testspace_rest_query.testspace_post('https://testsce.com/', payload)

    def test_space_result_set_enterprise(self):
        request_text = json.dumps(self.space_results_json[0])
        url = 'https://ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS:@abc.testspace.xyz.com/' \
              'api/projects/project1/spaces/space1/result_sets/8712'
        with requests_mock.Mocker() as mock:
            mock.get(url, text=request_text)
            response = testspace_rest_query.space_result_set('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                             'abc',
                                                             'project1',
                                                             'space1',
                                                             8712,
                                                             enterprise='xyz')
        assert response.status_code == 200
        result_set = result_set_api.TestspaceResultSetApi(response.json())
        assert int(url.split('/')[-1]) == result_set.get_result_id()
