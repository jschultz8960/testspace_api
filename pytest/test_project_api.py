import pytest
import requests_mock
from api import project_api
from api import testspace_rest_query
import json


def load_request_url(self):
    with open("pytest/projects.txt") as myfile:
        self.request_text = "".join(line.rstrip() for line in myfile)

    self.request_json = json.loads(self.request_text)
    self.request_url = 'https://ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS:@abc.testspace.com/api/projects'


class TestSpaceProjectApi():
    @pytest.fixture(autouse=True)
    def setup_class(self):
        load_request_url(self)

    def test_project_list(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                         'abc',
                                                         enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        project_list = results_sets.get_project_list()
        assert "testspace-samples:cpp.cpputest" in project_list

    def test_project_name_by_id(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                         'abc',
                                                         enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        assert 'testspace-samples:csharp.nunit' == results_sets.get_project_name_by_id(168)

    def test_project_name_by_invalid_id(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                         'abc',
                                                         enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        assert results_sets.get_project_name_by_id(100) is None

    def test_project_id_by_name(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                         'abc',
                                                         enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        assert 158 == results_sets.get_project_id_by_name('testspace-samples:ruby.rspec')

    def test_project_id_by_invalid_name(self):
        with requests_mock.Mocker() as mock:
            mock.get(self.request_url, text=self.request_text)
            response = testspace_rest_query.projects_get('ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                         'abc',
                                                         enterprise=None)
        results_sets = project_api.TestspaceProjectApi(response.json())
        assert results_sets.get_project_id_by_name('invalid_project_name') is None

    def test_create_project_by_name(self):
        with requests_mock.Mocker() as mock:
            project = self.request_json[0]
            payload = {}
            payload['name'] = project['name']
            payload['description'] = project['description']

            mock.post(self.request_url, status_code=201, json=payload)
            response = testspace_rest_query.projects_post('',
                                                          'ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                          'abc',
                                                          enterprise=None)
        assert response.status_code == 201
        response_json = json.loads(response.text)
        assert response_json['name'] == payload['name']

    def test_create_project_by_name_existing(self):
        with requests_mock.Mocker() as mock:
            project = self.request_json[0]
            payload = {}
            payload['name'] = project['name']
            payload['description'] = project['description']

            existing_project_json = json.loads(
                '{"message":"Name has already been taken by an existing Project (validation is case insensitive)"}')
            mock.post(self.request_url, status_code=422, json=existing_project_json)
            response = testspace_rest_query.projects_post('',
                                                          'ZytJu57XnK7clTweixNiHeZf10OaVFVaG8yKf4RS',
                                                          'abc',
                                                          enterprise=None)

        assert response.status_code == 422
        response_json = json.loads(response.text)
        assert response_json == existing_project_json
