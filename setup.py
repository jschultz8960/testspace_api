from setuptools import setup, find_packages

setup(
    name='testspace_api',
    version='',
    packages=find_packages(),
    url='',
    license='',
    author='jaschultz',
    author_email='',
    description='',
    install_requires=[
        'requests-mock',
        'requests',
        'pytest-cov',
    ]
)
